

## WordPress Index Cache
    Implementação para fazer cache no WordPress, porém esta implementação é executada antes do próprio WordPress, desta forma o cache se torna muito mais veloz, pois não haverá processamento no WordPress.

### Instalação

Edite o arquivo index.php, comente as funções do WordPress e adicione o código abaixo:

    require "wpicache/src/Wpicache.php";
    $wpicache = new Wpicache(__DIR__);
    $wpicache->timeToUpdate = 5; // 5 segundos
    $wpicache->theme = 'accesspress-ray';
    $wpicache->enabled = true;
    $wpicache->run();


    OBS: altere a propriedade theme para o nome do tema em uso.

#### Propriedades:

    theme:          (string)    tema em uso
    enabled:        (boolean)   habilita/ desabilita o cache
    timeToUpdate    (integer)   tempo de verificação de novas atualizações no WordPress
