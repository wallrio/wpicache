<?php


class View{

    public static $dirPlugin;


    public static function checkOptimizeBrowserCache($dirRoot){
        $htaccessFile = $dirRoot.'.htaccess';
        if(file_exists($htaccessFile)){
            $htaccessContent = file_get_contents($htaccessFile);

            if(strpos($htaccessContent,"# Wpicache Optimization ---- "."\n".'<FilesMatch "\.(ico|pdf|jpg|jpeg|png|gif|html|htm|xml|txt|xsl|js|css)$">'."\n".' Header set Cache-Control "max-age=31536050" '."\n".'</FilesMatch>') === false){
                    return false;
            }
        }

        return true;
    }

    public static function optimizeBrowserCache($dirRoot){
        $htaccessFile = $dirRoot.'.htaccess';
        if(file_exists($htaccessFile)){
            $htaccessContent = file_get_contents($htaccessFile);

            if(strpos($htaccessContent,"# Wpicache Optimization ---- "."\n".'<FilesMatch "\.(ico|pdf|jpg|jpeg|png|gif|html|htm|xml|txt|xsl|js|css)$">'."\n".' Header set Cache-Control "max-age=31536050" '."\n".'</FilesMatch>') === false){
                    $htaccessContent = $htaccessContent."\n"."# Wpicache Optimization ---- "."\n".'<FilesMatch "\.(ico|pdf|jpg|jpeg|png|gif|html|htm|xml|txt|xsl|js|css)$">'."\n".' Header set Cache-Control "max-age=31536050" '."\n".'</FilesMatch>';
                    file_put_contents($htaccessFile,$htaccessContent);
            }
        }
    }
    public static function removeOptimizeBrowserCache($dirRoot){
        $htaccessFile = $dirRoot.'.htaccess';
        if(file_exists($htaccessFile)){
            $htaccessContent = file_get_contents($htaccessFile);

            if(strpos($htaccessContent,"# Wpicache Optimization ---- "."\n".'<FilesMatch "\.(ico|pdf|jpg|jpeg|png|gif|html|htm|xml|txt|xsl|js|css)$">'."\n".' Header set Cache-Control "max-age=31536050" '."\n".'</FilesMatch>') !== false){
                    $htaccessContent = str_replace("\n"."# Wpicache Optimization ---- "."\n".'<FilesMatch "\.(ico|pdf|jpg|jpeg|png|gif|html|htm|xml|txt|xsl|js|css)$">'."\n".' Header set Cache-Control "max-age=31536050" '."\n".'</FilesMatch>','',$htaccessContent);
                    file_put_contents($htaccessFile,$htaccessContent);
            }
        }
    }

    public static function dirSize($dir)
    {
        $dirSize = 0;
        if(!is_dir($dir)){return false;};
        $files = scandir($dir);if(!$files){return false;}
        $files = array_diff($files, array('.','..'));

        foreach ($files as $file) {
            if(is_dir("$dir/$file")){
                 $dirSize += self::dirSize("$dir/$file");
            }else{
                $dirSize += filesize("$dir/$file");
            }
        }
        return $dirSize;
    }

    public static function rrmdir($dir) {
        if (is_dir($dir)) {
         $objects = scandir($dir);
         foreach ($objects as $object) {
           if ($object != "." && $object != "..") {
             if (is_dir($dir."/".$object))
               self::rrmdir($dir."/".$object);
             else
               unlink($dir."/".$object);
           }
         }
         rmdir($dir);
        }
    }

    public static function saveConfig($request){



        if(isset($request['enabled']))
            $enabled = true;
        else
            $enabled = false;


        $theme = isset($request['theme'])?$request['theme']:'';

        if(isset($request['minifyCss']))
            $minifyCss = true;
        else
            $minifyCss = false;

        if(isset($request['minifyJs']))
            $minifyJs = true;
        else
            $minifyJs = false;


        $timeToUpdate = isset($request['timeToUpdate'])?$request['timeToUpdate']:5;

        $activeKey = isset($request['activatekey'])?$request['activatekey']:'';

        $key_email = isset($request['key_email'])?$request['key_email']:'';
        $key_plan = isset($request['key_plan'])?$request['key_plan']:'';


        $array = array(
            'theme'=>$theme,
            'enabled'=>$enabled,
            'minifyCss'=>$minifyCss,
            'minifyJs'=>$minifyJs,
            'timeToUpdate'=>$timeToUpdate,
            'activeKey'=>$activeKey,

            'key_email'=>$key_email,
            'key_plan'=>$key_plan,
        );

        $filename = self::$dirPlugin.'config.json';

        file_put_contents($filename,json_encode($array));
    }

}
