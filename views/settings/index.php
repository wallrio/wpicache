<?php

$dirPlugin = dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR;
$dirRoot = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))).DIRECTORY_SEPARATOR;


require $dirPlugin.'views'.DIRECTORY_SEPARATOR.'View.php';
View::$dirPlugin = $dirPlugin;


    $styleCleanBt = '';
    if(!file_exists($dirPlugin.'objects')){
        $styleCleanBt = 'disabled';
    }


    // clean Cache ---------------------------------------------------
    $cacheDir = $dirPlugin.'objects';
    if(isset($_POST['clean'])){

        if(file_exists($cacheDir)){
            View::rrmdir($cacheDir);
            echo '<div style="display:table;margin-top:10px;margin-bottom:10px;background:#fff;color:green;padding:20px 40px">Cache clean!</div>';
        }
    }

    $cacheDir_size = View::dirSize($cacheDir);
    $cacheDir_size = number_format($cacheDir_size / 1048576, 1) . ' MB';





    // Active Key ---------------------------------------------------

    if(isset($_POST['active'])){

        $activeKey = isset($_POST['activatekey'])?$_POST['activatekey']:'';

        require $dirPlugin.'src'.DIRECTORY_SEPARATOR.'Wpicache_http.php';

        $resultKeys = Wpicache_http::curl(array(
        	'url'=> 'http://lavagemdecarpeteaseco.com.br/wp-content/plugins/wpicache/keys.json'
        ));

        $resultKeys = json_decode($resultKeys);
        $ifactive = false;
        if(is_object($resultKeys))
        foreach ($resultKeys as $key => $value) {



            if($key == $activeKey){
                $ifactive = true;

                $key_email = $value->email;
                $key_plan = $value->plan;

                break;
            }
        }



        if($ifactive == true){
            $_POST['activatekey_check'] = $activeKey;

            $_POST['key_email'] = $key_email;
            $_POST['key_plan'] = $key_plan;

            View::saveConfig($_POST);
            echo '<div style="display:table;margin-top:10px;margin-bottom:10px;background:#fff;color:green;padding:20px 40px">Activate with success!</div>';
        }else{
            $_POST['activatekey'] = '';
            $_POST['activatekey_check'] = '';
            echo '<div style="display:table;margin-top:10px;margin-bottom:10px;background:red;color:#fff;padding:20px 40px">Non Activate!</div>';
        }

    }




    // Save Config ---------------------------------------------------

    if(isset($_POST['save'])){

        $enabled = isset($_POST['enabled'])?$_POST['enabled']:false;


        if($enabled === true || $enabled === "on"){
            View::optimizeBrowserCache($dirRoot);
        }else{
            View::removeOptimizeBrowserCache($dirRoot);
        }


        $activeKey = isset($_POST['activatekey'])?$_POST['activatekey']:'';

        $activatekey_check = isset($_POST['activatekey_check'])?$_POST['activatekey_check']:'';
        if($activatekey_check == '')
        $_POST['activatekey'] = '';

        View::saveConfig($_POST);

        echo '<div style="display:table;margin-top:10px;margin-bottom:10px;background:#fff;color:green;padding:20px 40px">Saved with success!</div>';
    }

    $configPath = $dirPlugin.'config.json';
    if(file_exists($configPath)){
        $configContent = file_get_contents($configPath);
        $config = json_decode($configContent,true);

        $theme = isset($config['theme'])?$config['theme']:'';
        $enabled = isset($config['enabled'])?$config['enabled']:true;
        $minifyCss = isset($config['minifyCss'])?$config['minifyCss']:true;
        $minifyJs = isset($config['minifyJs'])?$config['minifyJs']:true;
        $timeToUpdate = isset($config['timeToUpdate'])?$config['timeToUpdate']:5;
        $activeKey = isset($config['activeKey'])?$config['activeKey']:'';

        $key_email = isset($config['key_email'])?$config['key_email']:'';
        $key_plan = isset($config['key_plan'])?$config['key_plan']:'';

        $optimize_cacheBrowser = 'No';

        if( View::checkOptimizeBrowserCache($dirRoot) == true ){
            $optimize_cacheBrowser = 'Yes';
        }

        $disabled_all = '';
        if($activeKey== ''){
            $disabled_all = ' disabled ';
        }

        $enabled_checked = '';
        $minifyCss_checked = '';
        $minifyJs_checked = '';
        $activeKeyStyle = '';

        if($enabled === true){
            $enabled_checked = ' checked ';
        }
        if($minifyCss === true){
            $minifyCss_checked = ' checked ';
        }
        if($minifyJs === true){
            $minifyJs_checked = ' checked ';
        }

        if($activeKey != ''){
            $activeKeyStyle = ' disabled ';
        }

    }





?>

<style media="screen">
@media (min-width:600px){
    .line{
        display: table;
    }.col{
        display: table-cell;
        width: 49%;
    }
}



.form-table{

}.form-table tr{
    border-bottom: 1px solid #ccc;
}.form-table tr th{
    padding: 10px;
    background: #fff;
    color: #777;
}.form-table tr td{
    background: #e1e1e1;
}
.buttonFat{
    padding: 10px 30px !important;
    height: auto !important;
}
</style>
<form method="post">

    <input name="key_email" id="key_email"  type="hidden" value="<?php echo $key_email; ?>" >
    <input name="key_plan" id="key_plan"  type="hidden" value="<?php echo $key_plan; ?>" >


<div class="wrap">
    <h1>WPIcache</h1>

    <div class="line">
        <div class="col">


            <table class="form-table">



                <tr>
                    <th scope="row">Enable</th>
                    <td>
                        <fieldset>
                            <legend class="screen-reader-text"><span>Enable</span></legend><label for="">
                            <input name="enabled" id="enabled"  type="checkbox" <?php echo $enabled_checked; ?> <?php echo $disabled_all; ?> >
                            </label>
                        </fieldset>
                    </td>
                </tr>


                <tr>
                    <th scope="row"><label for="blogname">Activate Key</label></th>
                    <td>
                        <fieldset>
                            <legend class="screen-reader-text"><span>Activate Key</span></legend><label for="">
                            <input name="activatekey" id="activatekey"  type="text" value="<?php echo $activeKey; ?>" >
                            <input name="activatekey_check" id="activatekey_check"  type="hidden" value="<?php echo $activeKey; ?>" >

                            <input name="active" id="active" class="button button-primary" value="Active Now" type="submit" <?php echo $activeKeyStyle; ?> >

                            </label>
                            <br />
                            <?php if($activeKey!= ''): ?>
                                <label>Registred: <strong><?php echo $key_email; ?></strong> - Plan: <strong><?php echo $key_plan; ?> domains</strong></label>
                            <?php else:  ?>
                                <a href="http://wallrio.com/wordpress/plugins/wpicache/getactivekey" target="_blank">Click here to get key of activation</a>
                            <?php endif;  ?>

                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <th scope="row"><label for="blogname">Theme</label></th>
                    <td><input name="theme" id="theme" value="<?php echo $theme; ?>" class="regular-text" type="text" readonly>
                    </td>
                </tr>


                <tr>
                    <th scope="row"><label for="blogname">Time To Update</label></th>
                    <td><input name="timeToUpdate" id="timeToUpdate" value="<?php echo $timeToUpdate; ?>" class="regular-text" type="text" <?php echo $disabled_all; ?>>
                        <p class="description" id="home-description">Update scan time</p>
                    </td>
                </tr>

                <tr>
                    <th scope="row">To minify Css</th>
                    <td>
                        <fieldset>
                            <legend class="screen-reader-text"><span>To minify Css</span></legend><label for="">
                            <input name="minifyCss" id="minifyCss"  type="checkbox" <?php echo $minifyCss_checked; ?> <?php echo $disabled_all; ?> >
                            </label>
                        </fieldset>
                    </td>
                </tr>


                <tr>
                    <th scope="row">To minify Js</th>
                    <td>
                        <fieldset>
                            <legend class="screen-reader-text"><span>To minify Js</span></legend><label for="">
                            <input name="minifyJs" id="minifyJs"  type="checkbox" <?php echo $minifyJs_checked; ?> <?php echo $disabled_all; ?>>
                            </label>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <th scope="row">Action to cache</th>
                    <td>
                        <fieldset>
                            <legend class="screen-reader-text"><span>Action to cache</span></legend><label for="">
                            <input name="clean" id="clean" class="button button-primary " value="Clean" type="submit" <?php echo $styleCleanBt; ?> <?php echo $disabled_all; ?>>
                            <p class="description" id="home-description">Size current: <?php echo $cacheDir_size; ?></p>
                            </label>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <th scope="row">Optimize Cache Browser</th>
                    <td>
                        <fieldset>
                            <legend class="screen-reader-text"><span>Optimize Cache Browser</span></legend><label for="">
                            <?php echo $optimize_cacheBrowser; ?>
                            </label>
                        </fieldset>
                    </td>
                </tr>


                </tbody>
            </table>

            <Br />
            <input name="save" id="save" class="button button-primary buttonFat" value="Save" type="submit" <?php echo $disabled_all; ?>>

        </div>
        <div class="col" style="padding-left:40px">

            <h2>Tips</h2>

            <p>1. The cache is to build in first access on page</p>
            <p>2. The cache is updated in each modification on administration</p>
            <p>3. The htaccess is modified to cache browser</p>

        </div>

    </div>
</div>

</form>
