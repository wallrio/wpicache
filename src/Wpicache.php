<?php
/**
 * WordPress Index Cache
 * by Wallace Rio -  wallrio.com - wallrio@gmail.com
 */


require_once dirname(__FILE__).DIRECTORY_SEPARATOR."Wpicache_wpload.php";

require_once dirname(__FILE__).DIRECTORY_SEPARATOR."Wpicache_minify.php";
require_once dirname(__FILE__).DIRECTORY_SEPARATOR."Wpicache_http.php";

class Wpicache {

    private $dir;
    public  $enabled = false,
            $theme,
            $timeToUpdate,
            $activeKey;

    function __construct($dir){
        $this->dir = $dir;


    }

    public function defaultWordPress(){
        define('WP_USE_THEMES', true);
        $dirRoot = dirname(dirname(dirname(dirname(dirname( __FILE__ )))));
        require_once( $dirRoot . DIRECTORY_SEPARATOR . 'wp-blog-header.php' );

        // echo file_get_contents($dirRoot . DIRECTORY_SEPARATOR . 'wp-blog-header.php');
    }

    public function checkPage(){

        $REQUEST_URI = isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:null;
        $REQUEST_URIArray = explode('?',$REQUEST_URI);
        $page = $REQUEST_URIArray[0];

        // $dir = getcwd().DIRECTORY_SEPARATOR . wpicache . DIRECTORY_SEPARATOR .'objects'.DIRECTORY_SEPARATOR;
        $dir = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR  .'objects'.DIRECTORY_SEPARATOR;

        $dirFinal = $dir.$page;
        $dirFinal = str_replace('//','/',$dirFinal);


        if(file_exists($dirFinal.'index.html'))
            return true;

        return false;
    }

    public function savePage($content){

        $REQUEST_URI = isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:null;
        $REQUEST_URIArray = explode('?',$REQUEST_URI);
        $page = $REQUEST_URIArray[0];

        $dir = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR  .'objects'.DIRECTORY_SEPARATOR;
        // $dir = getcwd().DIRECTORY_SEPARATOR . wpicache . DIRECTORY_SEPARATOR .'objects'.DIRECTORY_SEPARATOR;
        $dirFinal = $dir.$page;
        $dirFinal = str_replace('//','/',$dirFinal);

        // if(substr($dirFinal,strlen($dirFinal)-1,strlen($dirFinal))=='/')
            // $dirFinal = substr($dirFinal,0,strlen($dirFinal)-1);
        // echo $dirFinal;

        if(!file_exists($dirFinal))
        mkdir($dirFinal,0777,true);
        file_put_contents($dirFinal.'index.html', $content);
    }

    public function getPage(){

        $REQUEST_URI = isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:null;
        $REQUEST_URIArray = explode('?',$REQUEST_URI);
        $page = $REQUEST_URIArray[0];

        $dir = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR  .'objects'.DIRECTORY_SEPARATOR;
        // $dir = getcwd().DIRECTORY_SEPARATOR . wpicache . DIRECTORY_SEPARATOR .'objects'.DIRECTORY_SEPARATOR;
        $dirFinal = $dir.$page;
        $dirFinal = str_replace('//','/',$dirFinal);



        return file_get_contents($dirFinal.'index.html');
    }

    public function checkActive(){


        $REQUEST_URI = isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:null;
        $REQUEST_URIArray = explode('?',$REQUEST_URI);
        $page = $REQUEST_URIArray[0];

        // set only home to cache
        /*if($page === '/'){
            return true;
        }*/

        $this->loadConfig();

        $activeKey = $this->activeKey;

        // apenas verifica se a chave consta no config.json,
        if($activeKey != '')
            return true;
        else
            return false;

        /*$resultKeys = Wpicache_http::curl(array(
        	'url'=> 'http://lavagemdecarpeteaseco.com.br/wp-content/plugins/wpicache/keys.json'
        ));

        $resultKeys = json_decode($resultKeys);
        $ifactive = false;
        if(is_object($resultKeys))
        foreach ($resultKeys as $key => $value) {

            if($key == $activeKey){
                $ifactive = true;
                break;
            }
        }

        if($ifactive == true){
            return true;
        }

        return false;*/

    }

    public function optimize($content){



        require "Wpicache_spider.php";



        ob_start();
        wp_head();
        $cacheHeader = ob_get_contents();
        ob_end_clean();


        ob_start();
        wp_footer();
        $cacheFooter = ob_get_contents();
        ob_end_clean();


        /*$dir = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR  .'tests'.DIRECTORY_SEPARATOR.'header.html';
        $cacheHeader = file_get_contents($dir);

        $dir = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR  .'tests'.DIRECTORY_SEPARATOR.'footer.html';
        $cacheFooter = file_get_contents($dir);*/

        // file_put_contents('aaa-header.txt',$cacheHeader);
        // file_put_contents('aaa-footer.txt',$cacheFooter);


        $addFile = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'add'.DIRECTORY_SEPARATOR.'add.html';
        if( file_exists($addFile) ){
            $addHtmls = file_get_contents($addFile);
            $cacheHeader = $addHtmls.$cacheHeader;
        }


        $spider = new Wpicache_spider();
        $cacheHeader_optimied = $spider->run($cacheHeader);
        // exit;

        $cacheFooter_optimied = $spider->run($cacheFooter);




        if( substr($this->theme, strlen($this->theme)-1, strlen($this->theme) ) == '/' ){
            $this->theme = substr($this->theme, 0,strlen($this->theme)-1 );
        }

        $themeName = $this->theme;
        $themeNameArray = explode('/',$themeName);
        $themeName = end($themeNameArray);

        $cacheHeader_optimied  = str_replace('url(../','url(wp-content/themes/'.$themeName.'/',$cacheHeader_optimied);
        $cacheHeader_optimied  = str_replace('url(../','url(wp-content/themes/'.$themeName.'/',$cacheHeader_optimied);



        if($this->checkActive() === true){
            $content = str_replace('{wpicache:header}',$cacheHeader_optimied,$content);
            $content = str_replace('{wpicache:footer}',$cacheFooter_optimied,$content);
            $content = Wpicache_minify::html($content);

            $this->savePage($content);
        }else{
            $content = str_replace('{wpicache:header}',$cacheHeader,$content);
            $content = str_replace('{wpicache:footer}',$cacheFooter,$content);
        }




        return $content;
    }

    public function runCache(){

        $this->wpload->setTimeToUpdate($this->timeToUpdate);

        if($this->checkPage() == true && $this->wpload->updated() === false){

            echo $this->getPage();
            return true;
        }

        // $themeDir = getcwd(). DIRECTORY_SEPARATOR .'wp-content'.DIRECTORY_SEPARATOR.'themes' . DIRECTORY_SEPARATOR .$this->theme . DIRECTORY_SEPARATOR;
        $themeDir = $this->theme . DIRECTORY_SEPARATOR;

        // echo $themeDir;

        $headerFile = $themeDir . 'header.php';
        $footerFile = $themeDir . 'footer.php';
        $indexFile = $themeDir . 'index.php';


        $headerContent = file_get_contents($headerFile);
        $footerContent = file_get_contents($footerFile);

        /*if(file_exists($headerFile) && !file_exists($headerFile.'copy-wpicache') ){

            copy($headerFile,$headerFile.'copy-wpicache');
            $headerContent = file_get_contents($headerFile);
            $headerContent = str_replace('wp_head();', ' ?>{wpicache:header}<?php ' ,$headerContent);
            file_put_contents($headerFile,$headerContent);
        }

        if(file_exists($footerFile) && !file_exists($footerFile.'copy-wpicache') ){
            copy($footerFile,$footerFile.'copy-wpicache');
            $footerContent = file_get_contents($footerFile);
            $footerContent = str_replace('wp_footer();', ' ?>{wpicache:footer}<?php ' ,$footerContent);
            file_put_contents($footerFile,$footerContent);
        }*/



        ob_start();
        $this->defaultWordPress();
        $content = ob_get_contents();
        ob_clean();

        /*$dir = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR  .'tests'.DIRECTORY_SEPARATOR.'index.html';
        $content = file_get_contents($dir);*/

        // restaure header.php and footer.php
        /*if(file_exists($headerFile.'copy-wpicache') ){
            copy($headerFile.'copy-wpicache',$headerFile);
            unlink($headerFile.'copy-wpicache');
        }
        if(file_exists($footerFile.'copy-wpicache') ){
            copy($footerFile.'copy-wpicache',$footerFile);
            unlink($footerFile.'copy-wpicache');
        }
*/



        $content = $this->optimize($content);







        echo $content;

        return $themeDir;
    }


    public function loadConfig(){
        $dirRoot = dirname(dirname( __FILE__ ));

        $configFile = $dirRoot . DIRECTORY_SEPARATOR . 'config.json';
        if(file_exists($configFile)){
            $configContent = file_get_contents($configFile);
            $config = json_decode($configContent);

            $this->theme = isset($config->theme)?$config->theme:null;
            $this->enabled = isset($config->enabled)?$config->enabled:null;
            $this->minifyCss = isset($config->minifyCss)?$config->minifyCss:null;
            $this->minifyJs = isset($config->minifyJs)?$config->minifyJs:null;
            $this->timeToUpdate = isset($config->timeToUpdate)?$config->timeToUpdate:null;
            $this->activeKey = isset($config->activeKey)?$config->activeKey:null;
        }



    }

    public function run(){

        $this->loadConfig();

        $this->wpload = new Wpicache_wpload($this->theme);

        $this->wpload->writeOnFunctions();


        if($this->enabled === false || $this->activeKey == ""){



            ob_start();
            $this->defaultWordPress();
            $content = ob_get_contents();
            ob_clean();

            ob_start();
            wp_head();
            $cacheHeader = ob_get_contents();
            ob_end_clean();

            ob_start();
            wp_footer();
            $cacheFooter = ob_get_contents();
            ob_end_clean();



            $content = str_replace('{wpicache:header}',$cacheHeader,$content);
            $content = str_replace('{wpicache:footer}',$cacheFooter,$content);

            echo $content;

            return false;
        }


        return $this->runCache();
    }

}
