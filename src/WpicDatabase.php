<?php

class WpicDatabase{

    function __construct(){
        $access = $this->getAccessFromWpConfig();
        if( $access !== false ){
            $host = $access['host'];
            $base = $access['base'];
            $dsn = 'mysql:host='.$host.';dbname='.$base;
            $username = $access['username'];
            $password  = $access['password'];
            $driver_options = null;

            // $db = new PDO($dsn,$username,$password, $driver_options);




        }


    }


    public function getDefineDeclarate($variable,$context){
        $pattern = '/define\(\''.$variable.'\', (.*)\);/m';
        $resultPreg = preg_match_all($pattern, $context,$match);

        if(count($match[1]) < 1 ){
            $pattern = '/define\("'.$variable.'", (.*)\);/m';
            $resultPreg = preg_match_all($pattern, $context,$match);
        }

        $result = $match[1][0];
        $result = substr($result, 1);
        $result = substr($result, 0,strlen($result)-1);

        return $result;
    }


    public function getAccessFromWpConfig(){
        $wpConfigFile = 'wp-config.php';
        if(file_exists($wpConfigFile)){
            $wpConfigContent = file_get_contents($wpConfigFile);
            $host = $this->getDefineDeclarate('DB_HOST',$wpConfigContent);
            $base = $this->getDefineDeclarate('DB_NAME',$wpConfigContent);
            $username = $this->getDefineDeclarate('DB_USER',$wpConfigContent);
            $password = $this->getDefineDeclarate('DB_PASSWORD',$wpConfigContent);
            return array(
                'host'  =>  $host,
                'base'  =>  $base,
                'username'  =>  $username,
                'password'  =>  $password,
            );
        }

        return false;
    }
}
