<?php
/**
 * WordPress Index Cache
 * by Wallace Rio -  wallrio.com - wallrio@gmail.com
 */


require "Wpicache_http.php";
require "Wpicache_minify.php";

class Wpicache_spider extends Wpicache_http{

	private $url;


	function __construct(){}







	public function run($content){



		// preg_match_all('/<link[^>]*?rel=["|\']stylesheet["|\']*?href=["|\'](.*?\.css)["|\']*?[^>]*>/is', $content, $matchesStyleLink);
		preg_match_all('/<link[^>]*?rel=["|\']stylesheet["|\']*?[^>]*>/is', $content, $matchesStyleLink);
		// preg_match_all('/<script[^>]*?src=["|\'](.*?\.js)["|\']*?[^>]*>/is', $content, $matchesScriptLink);
		preg_match_all('#<style[^<]+?>(.*?)</style>#is', $content, $matchesStyleInline);
		preg_match_all('#<script[^<]+?>(.*?)</script>#is', $content, $matchesScriptInline);

		/*print_r($matchesStyleLink);

		echo 11;
		exit;*/
		$contentNew = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $content);
		$contentNew = preg_replace('/<link\b[^>]* ?(rel="sheetstyle")? ?>/is', "", $contentNew);

		$matchesScripts = array();
		$matchesStyle = array();
		$matchesStyleContent = array();
		$matchesStyleContentPre = array();

		$indexScript = 0;
		$indexStyle = 0;


		$html_styleInline = '';
		foreach ($matchesStyleInline[0] as $key => $value) {
			// $html_styleInline .=  "\n".'<style type="text/css" data-wpic-type="inline" data-index="'.$key.'" >';
				$html_styleInline .= Wpicache_minify::css($value);
				// $html_styleInline .= ($value);
			// $html_styleInline .= '</style>';
		}




		$html_scripts = '';
		foreach ($matchesScriptInline[0] as $key => $value) {

			$valueNew = preg_match_all('/src=["|\'](.*?)["|\']/is',$value,$newMatch);
			$attrStringsPre = preg_replace('/src=["|\'](.*?)["|\']/is','',$value);
			$valueNew = preg_match_all('/<script\b(.*?)*>/is',$attrStringsPre,$newMatch2);
			$attrStrings = str_replace(array('<script','/>','>'),'',$newMatch2[0][0]);


			if(count($newMatch[0]) > 0 ){
				$url = $newMatch[1][0];
				$valueResult = '';
				$valueResult .= "\n".'<script '.$attrStrings .' data-wpic-type="link" >';
				$contentFile = Wpicache_http::curl(array(
						'url'=>$url,
						'method'=>'get'
					));
				$contentFile = str_replace('<iframe','&lt;iframe',$contentFile);
				$contentFile = str_replace('</script','&lt;/',$contentFile);
				$valueResult .= Wpicache_minify::js($contentFile);
				$valueResult .= "\n".'</script>';

			}else{
				$valueResult = $value;
				$valueResult = strip_tags($valueResult);
				$valueResult = Wpicache_minify::js($valueResult);
				if($valueResult == "") continue;
				$valueResult = '<script '.$attrStrings.' data-wpic-type="inline" >'.$valueResult."\n".'</script>';
			}




			$html_scripts .= $valueResult;

		}



		// get style (link) remote
		$html_style = '';
		foreach ($matchesStyleLink[0] as $key => $value) {

			$attrStringsPre = preg_replace('/href=["|\'](.*?)["|\']/is','',$value);
			$attrStrings = str_replace(array('<link','/>','>'),'',$attrStringsPre);
			// $valueNew2 = preg_match_all('/<(.*?)>/is',$valueNew,$newMatch2);
			$valueNew = preg_match_all('/href=["|\'](.*?)["|\']/is',$value,$newMatch);
			$url = $newMatch[1][0];



			$html_style .= "\n".'<style '.$attrStrings .' >';
			$contentFile = Wpicache_http::curl(array(
					'url'=>$url,
					'method'=>'get'
				));



			$html_style .= Wpicache_minify::css($contentFile);
			$html_style .= '</style>';
		}



		// get script (link) remote


		/*foreach ($matchesScriptLink[0] as $key => $value) {

			$attrStringsPre = preg_replace('/src=["|\'](.*?)["|\']/is','',$value);
			$attrStrings = str_replace(array('<script','/>','>'),'',$attrStringsPre);
			// $valueNew2 = preg_match_all('/<(.*?)>/is',$valueNew,$newMatch2);
			$valueNew = preg_match_all('/src=["|\'](.*?)["|\']/is',$value,$newMatch);
			$url = $newMatch[1][0];



			$html_script .= "\n".'<script '.$attrStrings .' >';
			$contentFile = Wpicache_http::curl(array(
					'url'=>$url,
					'method'=>'get'
				));



			$html_script .= Wpicache_minify::js($contentFile);
			$html_script .= '</script>';
		}
*/

		ob_start();

		echo $contentNew;
		echo $html_style;
		echo $html_scripts;
		echo $html_styleInline;

		$contentFinish = ob_get_contents();
		ob_end_clean();

		// echo $contentFinish;
		// exit;
			// $html_scriptInline .= ' </script> ';


		// $contentFinish = $html_style . $html_script.$html_styleInline;
		// $contentFinish = $html_style . $html_script . $html_scriptInline . $html_styleInline;
		// $contentFinish = $html_style . $html_script . $html_scriptInline . $html_styleInline;
		// $contentFinish = $contentNew.$html_style . $html_script . $html_scriptInline . $html_styleInline;
		// $contentFinish = $contentNew.$html_style . $html_script . $html_scriptInline . $html_styleInline;


		 return $contentFinish;


	}

}
