<?php
/**
 * WordPress Index Cache
 * by Wallace Rio -  wallrio.com - wallrio@gmail.com
 */

class Wpicache_wpload{

    private $timeToUpdate,$theme;

    function __construct($theme){
        $this->theme = $theme;
    }

    /**
     * verifica se existe atualizações
     */
    public function updated(){
        $wpicacheDir = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR;
        $fileUpdatedPath = $wpicacheDir . 'lastupdate.dat';
        $timeToUpdate = $this->timeToUpdate;
        if(file_exists($fileUpdatedPath)){
            $fileUpdated = file_get_contents($fileUpdatedPath);
            if( strtotime(Date('Y/d/m H:i:s')) < strtotime($fileUpdated)+ $timeToUpdate  ){
                date_default_timezone_set('Brazil/East');
                $content = date("Y/d/m H:i:s", strtotime($fileUpdated)- $timeToUpdate );
                file_put_contents($fileUpdatedPath,$content);
                return true;
            }
        }
        return false;
    }

    /**
     * define o tempo de checagem de atualizações
     */
    public function setTimeToUpdate($timeToUpdate){
        $this->timeToUpdate = $timeToUpdate;
    }

    /**
     * adiciona no functions.php função de monutiramento de edições
     */
    public function writeOnFunctions(){

        // $directoryTheme = getcwd().DIRECTORY_SEPARATOR .'wp-content/themes/'.$this->theme;
        $directoryTheme = $this->theme;
        $directoryFunctions = $directoryTheme.DIRECTORY_SEPARATOR.'functions.php';

        if(file_exists($directoryFunctions)){
            $functionsContent = file_get_contents($directoryFunctions);
            if( strpos($functionsContent,'wpicacheOnsave') === false ){
                $wpicacheDir = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR;
                $code = "function wpicacheOnsave(){date_default_timezone_set('Brazil/East');file_put_contents('".$wpicacheDir."'.'lastupdate.dat', Date('Y/d/m H:i:s'));}add_action( 'save_post', 'wpicacheOnsave' );";
                $functionsContent = $functionsContent."\n".$code;
                file_put_contents($directoryFunctions,$functionsContent);
            }
        }
    }

}
