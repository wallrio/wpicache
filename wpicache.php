<?php
/*
Plugin Name: WPIcache
Plugin URI:
Description: Tool for optimization, Cache dynamic of pages
Version: 1.0
Author: Wallace Rio
Author URI: http://wallrio.com
License: GPLv2
*/
if(!class_exists('WPIcache')){
class WPIcache{

    private $rootDir,
            $pluginDir,
            $backupDir,
            $wpcontentDir,
            $uploadDir,
            $themesDir;

    function __construct(){

        register_activation_hook(__FILE__ , array($this, 'enable'));
        register_deactivation_hook(__FILE__ , array($this, 'disable'));

        $this->pluginDir = dirname(__FILE__) . DIRECTORY_SEPARATOR;
        $this->rootDir = ((dirname(dirname(dirname(dirname(__FILE__)))))) . DIRECTORY_SEPARATOR;
        $this->wpcontentDir = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR;
        $this->uploadDir = $this->wpcontentDir . 'uploads' . DIRECTORY_SEPARATOR;
        $this->themesDir = $this->wpcontentDir . 'themes' . DIRECTORY_SEPARATOR;
        $this->backupDir = $this->pluginDir . 'backup' . DIRECTORY_SEPARATOR;

         add_action('admin_menu', array('WPIcache','makeMenu'));
    }

    public function makeMenu(){
        $plugin_icon_path = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4gIPEB4Ck5KunAAAAdNJREFUOMu1lDFI1VEUxn8n/6WmRG/IIRIkKOxFQksODUHQ5FBDBEUNbUXttjhLQ2M0BiUEBVFLQQXWVNEYNZnW4w2Smhj2DPP5a+g8eDyU11AHLhzuuee75/vOuRf+kcVmAbUf2AOUgDowB1QiYv6vgNRDwAhwDBgAtmXoOzAFPAWeRUR1swp61dPqpPpTXVDfqUvqtPpBranf1Lvq0Y1ACvWC+jETHyToYfWmelU9ol5SX6l19aU63Ao0rL5W59RxdZ8aDa3Unel3qmV1Ql1V7zRiqD3qDf9YRT3RrkPqlayqpp4D2AIcSGFngccpaDubBh4C3cBJtQv1bAp7Xx1sUMqbt6pD6oDa0VJVWZ1Rp9T+IuekMyupRIRN50eAsfSr6iKwkOttsigDfUXSA1gFbKFQAvYD60AB7AC252x1ZQ5AFMCPBNidnFeagB4Bi3lmBlgGfiVACdgF1IAl1OPqJ/WNekrtbtFio+nfq15UV9Tnai9qn3ov219Vz6sduX8wxR5KP3KWrquzmTOqRhERX9Xb+SjfA0/y4mvAmdTCfJeXs+23kmYJmIgIi0x6AUwC6xFRVwtgHvicwkbqUkv/CzCe/lq7b6QnO9VsyxFR53/ab1nXkvM52TvSAAAAAElFTkSuQmCC';
		add_menu_page('WPIcache', 'WPIcache','manage_options', 'wpicache',array('WPIcache','managerContent'),$plugin_icon_path);
		// add_submenu_page('wpicache', 'Settings', 'settings', 'manage_options', 'wpicache_settings',array('WPIcache','settingsContent'));
		add_submenu_page('wpicache', 'About', 'about', 'manage_options', 'wpicache_about',array('WPIcache','aboutContent'));
    }
    public static function managerContent(){
        $viewsDir = dirname(__FILE__).DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR;
        $viewCurrentDir = $viewsDir.'settings'.DIRECTORY_SEPARATOR;

        ob_start();
        require $viewCurrentDir.'index.php';
        $content = ob_get_contents();
        ob_clean();

        echo $content;
    }
    public static function aboutContent(){
        $viewsDir = dirname(__FILE__).DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR;
        $viewCurrentDir = $viewsDir.'about'.DIRECTORY_SEPARATOR;

        ob_start();
        require $viewCurrentDir.'index.php';
        $content = ob_get_contents();
        ob_clean();

        echo $content;
    }
    /*public static function settingsContent(){
        $viewsDir = dirname(__FILE__).DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR;
        $viewCurrentDir = $viewsDir.'settings'.DIRECTORY_SEPARATOR;

        ob_start();
        require $viewCurrentDir.'index.html';
        $content = ob_get_contents();
        ob_clean();

        echo $content;


	}*/

    public function enable(){
        $WPIcache_loadfile = $this->pluginDir.'src'.DIRECTORY_SEPARATOR.'Wpicache_load.php';
        $indexFile = $this->rootDir.'index.php';
        $indexContent = file_get_contents($indexFile);



        /*if( function_exists('get_current_theme')){
            $current_theme =  get_current_theme();
        }else{
            $current_theme = wp_get_theme();
            $current_theme  = esc_html( $current_theme->get( 'TextDomain' ) );
        }*/

        $current_theme = get_template_directory() . DIRECTORY_SEPARATOR;

        $optionsCache = array(
            'theme'=>$current_theme,
            'enabled' => true,
            'minifyCss'=>true,
            'minifyJs'=>true,
            'timeToUpdate'=>5
        );
        file_put_contents($this->pluginDir.'config.json',json_encode($optionsCache));


        if(strpos($indexContent,'require_once "'.$WPIcache_loadfile.'";') === false){
            $indexContent = '<?php require_once "'.$WPIcache_loadfile.'"; ?>' . $indexContent;
            file_put_contents($indexFile,$indexContent);
        }






        $headerFile = $current_theme . 'header.php';
        $footerFile = $current_theme . 'footer.php';

        // if(file_exists($headerFile) && !file_exists($headerFile.'copy-wpicache') ){

            copy($headerFile,$headerFile.'copy-wpicache');
            $headerContent = file_get_contents($headerFile);
            $headerContent = str_replace('wp_head();', ' ?>{wpicache:header}<?php ' ,$headerContent);
            file_put_contents($headerFile,$headerContent);
        // }

        // if(file_exists($footerFile) && !file_exists($footerFile.'copy-wpicache') ){
            copy($footerFile,$footerFile.'copy-wpicache');
            $footerContent = file_get_contents($footerFile);
            $footerContent = str_replace('wp_footer();', ' ?>{wpicache:footer}<?php ' ,$footerContent);
            file_put_contents($footerFile,$footerContent);
        // }


	}

	public function disable(){
        $WPIcache_loadfile = $this->pluginDir.'src'.DIRECTORY_SEPARATOR.'Wpicache_load.php';
        $indexFile = $this->rootDir.'index.php';
        $indexContent = file_get_contents($indexFile);
        if(strpos($indexContent,'require_once "'.$WPIcache_loadfile.'";') !== false){
            $indexContent = str_replace('<?php require_once "'.$WPIcache_loadfile.'"; ?>' ,'', $indexContent);
            file_put_contents($indexFile,$indexContent);
        }




        $current_theme = get_template_directory() . DIRECTORY_SEPARATOR;

        $headerFile = $current_theme . 'header.php';
        $footerFile = $current_theme . 'footer.php';

        copy($headerFile.'copy-wpicache',$headerFile);
        copy($footerFile.'copy-wpicache',$footerFile);

        unlink($headerFile.'copy-wpicache');
        unlink($footerFile.'copy-wpicache');

	}

}

new WPIcache;
}
